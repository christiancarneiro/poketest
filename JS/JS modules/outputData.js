// Imprime os dados de uma lista de objetos de pokemón
const outputPokemonList = (pokemonList) => {
  console.log("\nAqui está o resultado:");

  pokemonList.forEach((pokemon, index) => {
    console.log(`\n${index}.`);
    console.log(pokemon.name, "\nSaiba mais: ", pokemon.url);
  });

  console.log("\nA GameFreak tá indo longe demais...");
};

// Imprime os dados de um objeto de pokemon
const outputFoundPokemonData = (pokemonData) => {
  if (pokemonData) {
    console.log("\n================================================");
    console.log(`Sobre o ${pokemonData.name}:`);
    console.log("Id geral:", pokemonData.generalId);
    console.log("Id local:", pokemonData.listId);
    console.log("Saiba mais:", pokemonData.url);
    console.log("================================================");
  } else {
    console.log("================================================");
    console.log("\nPokémon não encontrado.\n");
    console.log("================================================");
  }
};

module.exports = { outputPokemonList, outputFoundPokemonData };
