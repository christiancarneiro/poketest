// procura um pokemón em uma lista
const hasPokemon = (list, pokemonName) => {
  const nameList = list.map((pokemon) => pokemon.name);
  return nameList.includes(pokemonName);
};

// adiciona um objeto de pokemon a uma lista, se esta ainda não o houver. Retorna a lista resultante.
const addPokemon = (list, pokemonObject) => {
  console.log(`\nAdicionando ${pokemonObject.name}...\n`);
  if (hasPokemon(list, pokemonObject.name) === false) {
    console.log("Sucesso!!");
    list.push(pokemonObject);
  } else {
    console.log(
      "A operação falhou. A clonagem de pokemóns não é permitida pela OMS."
    );
  }

  return list;
};

// remove um objeto de pokemon de uma lista, se esta ainda o houver. Retorna a lista resultante.
const removePokemon = (list, pokemonName) => {
  console.log(`\nRemovendo ${pokemonName}...\n`);
  if (hasPokemon(list, pokemonName)) {
    console.log("Sucesso!!");
    const index = list.indexOf(list.find((item) => item.name === pokemonName));
    list.splice(index, 1);
  } else {
    console.log("A operação falhou. Não se pode perder o que não se tem.");
  }

  return list;
};

// Renomeia um pokemon, caso ele esteja na lista.
const renamePokemon = (list, oldPokemonName, newPokemonName) => {
  console.log(
    `\nRebatizando o pokemón "${oldPokemonName}" => "${newPokemonName}"...\n`
  );
  if (hasPokemon(list, oldPokemonName)) {
    console.log("Sucesso!!");
    const index = list.indexOf(
      list.find((item) => item.name === oldPokemonName)
    );
    list[index].name = newPokemonName;
  } else {
    console.log(
      `A operação falhou. Não existe um pokemón chamado ${oldPokemonName} na lista fornecida.`
    );
  }

  return list;
};

// ordena a lista de pokemóns
const orderPokemonList = (list, order) => {
  console.log(`\nOrdenando a lista em ordem ${order}...`);
  if (order === "crescente") {
    console.log("Deu certo!");
    return list.sort((a, b) => a.name.localeCompare(b.name));
  } else if (order === "decrescente") {
    console.log("Deu certo!");
    return list.sort((a, b) => b.name.localeCompare(a.name));
  } else {
    console.log("O processo falhou. Erro de parâmetro.");
    return list;
  }
};

// encontra a interseção entre duas listas de pokemón
const intersectPokemonLists = (firstList, secondList) => {
  return firstList.filter((pokemon) =>
    secondList.some((secondpokemon) => pokemon.name === secondpokemon.name)
  );
};

// faz a união de 2 listas de pokemón
const unitePokemonLists = (firstList, secondList) => {
  return Array.from(new Set(firstList.concat(secondList)));
};

// será que é suficiente?
const multipleIntersectPokemonLists = (...pokemonList) => {
  let intersectLists = pokemonList[0];
  for (i = 1; i < pokemonList.length; i++) {
    intersectLists = intersectPokemonLists(intersectLists, pokemonList[i]);
  }

  return intersectLists;
};

module.exports = {
  hasPokemon,
  addPokemon,
  removePokemon,
  renamePokemon,
  orderPokemonList,
  intersectPokemonLists,
  unitePokemonLists,
  multipleIntersectPokemonLists,
};
