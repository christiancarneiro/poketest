const axios = require("axios");
const { hasPokemon } = require("./operationsData");

const getUrl = (type) => `https://pokeapi.co/api/v2/type/${type}`;

// nenhuma questão precisa da data cheia, mas vai que um dia precisa...
// faz o request da api, retorna todos os dados relacionados, de acordo com o tipo.
const getDataByType = async (type) => {
  const pokemonData = await axios
    .get(getUrl(type))
    .then((response) => {
      console.log(`Tipo ${type} encontrado com sucesso!`);
      return response;
    })
    .catch((err) => {
      console.log(
        `\nOps! Deu problema aí, ó: \n${err}\nNão deu pra achar pokemón do tipo ${type}, não (ò,..,ó)`
      );
    });

  return pokemonData;
};

// retorna uma lista de objetos de pokemóns de um dado tipo
const getPokemonByType = async (type) => {
  console.log(`\nBuscando pokemóns do tipo ${type}...`);

  const pokemonArray = await getDataByType(type).then((res) => {
    const foundPokemon = res.data.pokemon;
    return foundPokemon.map((item) => item.pokemon);
  });

  return pokemonArray;
};

// procura um pokemon (string) em uma lista {hasPokemon}, devolve info relativa a esse pokemon (caso este seja encontrado).
const foundPokemonData = (pokemonList, pokemonName) => {
  let foundPokemonInfo;
  if (hasPokemon(pokemonList, pokemonName)) {
    const foundPokemon = pokemonList.find((item) => item.name === pokemonName);
    const pokemonGeneralId = foundPokemon.url.split("/").reverse()[1]; // pesado demais?
    const pokemonListId = pokemonList.indexOf(foundPokemon);

    foundPokemonInfo = {
      name: foundPokemon.name,
      url: foundPokemon.url,
      generalId: pokemonGeneralId,
      listId: pokemonListId,
    };
  }

  return foundPokemonInfo;
};

module.exports = { getPokemonByType, getDataByType, foundPokemonData };
