const { getPokemonByType } = require("../JS modules/getData");
const { outputPokemonList } = require("../JS modules/outputData");
const {
  addPokemon,
  intersectPokemonLists,
  multipleIntersectPokemonLists,
} = require("../JS modules/operationsData");

const firstType = "dragon"; //lowercase
const secondType = "flying"; //lowercase
const thirdType = "ice"; //lowercase

main = async () => {
  const firstPokemonList = await getPokemonByType(firstType).catch((err) => {});
  const secondPokemonList = await getPokemonByType(secondType).catch(
    (err) => {}
  );
  const thirdPokemonList = await getPokemonByType(thirdType).catch((err) => {});
  if (firstPokemonList && secondPokemonList && thirdPokemonList) {
    //adicionando um pokemon qualquer que está nos dois primeiros tipos à terceira lista:
    const doubleTypePokemonObject = intersectPokemonLists(
      firstPokemonList,
      secondPokemonList
    )[0];
    const newThirdPokemonList = addPokemon(
      thirdPokemonList,
      doubleTypePokemonObject
    );

    const resultList = multipleIntersectPokemonLists(
      firstPokemonList,
      secondPokemonList,
      newThirdPokemonList
    );
    outputPokemonList(resultList);
  }
};

main();
