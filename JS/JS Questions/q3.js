const { getPokemonByType, foundPokemonData } = require("../JS modules/getData");
const { addPokemon } = require("../JS modules/operationsData");
const {
  outputPokemonList,
  outputFoundPokemonData,
} = require("../JS modules/outputData");

const type = "ground";
const pokemonObject = {
  name: "sandshrew",
  url: "https://pokeapi.co/api/v2/pokemon/27/",
};

main = async () => {
  const pokemonList = await getPokemonByType(type).catch((err) => {});
  if (pokemonList) {
    const addedList = addPokemon(pokemonList, pokemonObject);
    outputPokemonList(addedList);
    const pokemonData = foundPokemonData(pokemonList, pokemonObject.name);
    outputFoundPokemonData(pokemonData);
  }
};

main();
