const { getPokemonByType } = require("../JS modules/getData");
const { outputPokemonList } = require("../JS modules/outputData");

const type = "rsock";

main = async () => {
  const pokemonList = await getPokemonByType(type).catch((err) => {});
  if (pokemonList) outputPokemonList(pokemonList);
};

// tinha feito com then e catch, mas ficava pior de ler. Como fazer error handling nesses casos?

main();
