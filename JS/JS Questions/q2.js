const { getPokemonByType, foundPokemonData } = require("../JS modules/getData");
const { outputFoundPokemonData } = require("../JS modules/outputData");

const type = "rock"; //lowercase
const pokemon = "geodude"; //lowercase

main = async () => {
  const pokemonList = await getPokemonByType(type).catch((err) => {});
  if (pokemonList) {
    const pokemonData = foundPokemonData(pokemonList, pokemon);
    outputFoundPokemonData(pokemonData);
  }
};

main();
