const { getPokemonByType, foundPokemonData } = require("../JS modules/getData");
const { removePokemon } = require("../JS modules/operationsData");
const {
  outputPokemonList,
  outputFoundPokemonData,
} = require("../JS modules/outputData");

const type = "grass"; //lowercase
const pokemonName = "meganium"; //lowercase

// Fica melhor se eu mandar um null para o index removido?
main = async () => {
  const pokemonList = await getPokemonByType(type).catch((err) => {});
  if (pokemonList) {
    const pokemonData = foundPokemonData(pokemonList, pokemonName);
    outputFoundPokemonData(pokemonData);
    const removedList = removePokemon(pokemonList, pokemonName);
    outputPokemonList(removedList);
  }
};

main();
