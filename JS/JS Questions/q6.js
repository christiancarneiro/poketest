const { getPokemonByType } = require("../JS modules/getData");
const { outputPokemonList } = require("../JS modules/outputData");
const { orderPokemonList } = require("../JS modules/operationsData");

const type = "dragon"; //lowercase
const order = "crescente"; //lowercase

main = async () => {
  const pokemonList = await getPokemonByType(type).catch((err) => {});
  if (pokemonList) {
    const orderedList = orderPokemonList(pokemonList, order);
    outputPokemonList(orderedList);
  }
};

main();
