const { getPokemonByType } = require("../JS modules/getData");
const { outputPokemonList } = require("../JS modules/outputData");
const { unitePokemonLists } = require("../JS modules/operationsData");

const firstType = "dragon"; //lowercase
const secondType = "flying"; //lowercase

main = async () => {
  const firstPokemonList = await getPokemonByType(firstType).catch((err) => {});
  const secondPokemonList = await getPokemonByType(secondType).catch(
    (err) => {}
  );
  if (firstPokemonList && secondPokemonList) {
    const bothTypeList = unitePokemonLists(firstPokemonList, secondPokemonList);
    outputPokemonList(bothTypeList);
  }
};

main();
