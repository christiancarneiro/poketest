const { getPokemonByType, foundPokemonData } = require("../JS modules/getData");
const { renamePokemon } = require("../JS modules/operationsData");
const {
  outputPokemonList,
  outputFoundPokemonData,
} = require("../JS modules/outputData");

const type = "grass"; //lowercase
const oldPokemonName = "meganium"; //lowercase
const newPokemonName = "dinosalra"; //lowercase

main = async () => {
  const pokemonList = await getPokemonByType(type).catch((err) => {});
  if (pokemonList) {
    const pokemonData = foundPokemonData(pokemonList, oldPokemonName);
    outputFoundPokemonData(pokemonData);
    const renamePokemonList = renamePokemon(
      pokemonList,
      oldPokemonName,
      newPokemonName
    );
    outputPokemonList(renamePokemonList);
  }
};

main();
