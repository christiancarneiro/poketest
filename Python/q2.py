from py_modules import getData, outputData


type = 'rock'
pokemon_name = 'tyranitar'


def main(_type, _pokemon_name):

    pokemon_list = getData.get_pokemon_by_type(_type)
    found_pokemon_data = None
    if pokemon_list:
        found_pokemon_data = getData.find_pokemon_data(
            pokemon_list, _pokemon_name)
        outputData.output_pokemon_data(found_pokemon_data)

    return found_pokemon_data


main(type, pokemon_name)
