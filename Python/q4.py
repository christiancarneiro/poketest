from py_modules import getData, outputData, operationsData


type = 'ground'
pokemon_name = "sandshrew"


def main(_type, _pokemon_name):

    pokemon_list = getData.get_pokemon_by_type(_type)
    if pokemon_list:
        pokemon_data = getData.find_pokemon_data(
            pokemon_list, _pokemon_name)
        outputData.output_pokemon_data(pokemon_data)
        removed_list = operationsData.remove_pokemon(
            pokemon_list, _pokemon_name)
        outputData.output_pokemon_list(removed_list)


main(type, pokemon_name)
