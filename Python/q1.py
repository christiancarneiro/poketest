from py_modules import getData, outputData

type = 'rosck'  # lowercase


def main(_type):
    pokemon_list = getData.get_pokemon_by_type(_type)
    if pokemon_list:
        outputData.output_pokemon_list(pokemon_list)


main(type)
