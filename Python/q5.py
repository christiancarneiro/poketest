from py_modules import getData, outputData, operationsData


type = 'ground'
pokemon_old_name = "sandshrew"
pokemon_new_name = "tatu de areia"


def main(_type, _old_pokemon_name, _new_pokemon_name):

    pokemon_list = getData.get_pokemon_by_type(_type)
    if pokemon_list:
        pokemon_data = getData.find_pokemon_data(
            pokemon_list, _old_pokemon_name)
        outputData.output_pokemon_data(pokemon_data)
        rename_pokemon_list = operationsData.rename_pokemon(
            pokemon_list, _old_pokemon_name, _new_pokemon_name)
        outputData.output_pokemon_list(rename_pokemon_list)


main(type, pokemon_old_name, pokemon_new_name)
