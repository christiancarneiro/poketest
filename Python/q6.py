from py_modules import getData, outputData, operationsData


type = 'ground'
order = 'decrescente'


def main(_type, _order):

    pokemon_list = getData.get_pokemon_by_type(_type)
    if pokemon_list:
        ordered_list = operationsData.order_pokemon_list(pokemon_list, _order)
        outputData.output_pokemon_list(ordered_list)


main(type, order)
