from py_modules import getData, outputData, operationsData


first_type = 'dragon'
second_type = 'flying'
third_type = 'ice'


def main():

    first_pokemon_list = getData.get_pokemon_by_type(first_type)
    second_pokemon_list = getData.get_pokemon_by_type(second_type)
    third_pokemon_list = getData.get_pokemon_by_type(third_type)

    if first_pokemon_list and second_pokemon_list and third_pokemon_list:
        double_type_pokemon_object = operationsData.intersect_pokemon_lists(
            first_pokemon_list, second_pokemon_list)[0]
        new_third_pokemon_list = operationsData.add_pokemon(
            third_pokemon_list, double_type_pokemon_object)

        result_list = operationsData.multiple_intersect_pokemon_lists(
            first_pokemon_list, second_pokemon_list, new_third_pokemon_list)
        outputData.output_pokemon_list(result_list)


main()
