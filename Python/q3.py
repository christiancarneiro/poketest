from py_modules import getData, outputData, operationsData


type = 'ground'
pokemon_object = {
    'name': "sandshrew",
    'url': "https://pokeapi.co/api/v2/pokemon/27/",
}


def main(_type, _pokemon_object):

    pokemon_list = getData.get_pokemon_by_type(_type)
    if pokemon_list:
        added_list = operationsData.add_pokemon(pokemon_list, _pokemon_object)
        outputData.output_pokemon_list(added_list)
        pokemon_data = getData.find_pokemon_data(
            pokemon_list, pokemon_object['name'])
        outputData.output_pokemon_data(pokemon_data)


main(type, pokemon_object)
