# Imprime os dados de uma lista de objetos de pokemón

def output_pokemon_list(_pokemon_list):
    print('\nAqui está o resultado: ')

    for pokemon in _pokemon_list:
        print(f'\n{_pokemon_list.index(pokemon)}.')
        print(pokemon['name'])
        print(f'Saiba mais: {pokemon["url"]}')

    print('\nA GameFreak tá indo longe demais...')


def output_pokemon_data(_pokemon_data):
    if _pokemon_data:
        print('\n===============================================')
        print(f'Sobre o {_pokemon_data["name"]}')
        print(f'Id geral: {_pokemon_data["general_id"]}')
        print(f'Id local: {_pokemon_data["list_id"]}')
        print(f'Saiba mais: {_pokemon_data["url"]}')
        print('===============================================\n')
    else:
        print('\n===============================================')
        print('Pokemón não encontrado')
        print('===============================================\n')
    print('\nA GameFreak tá indo longe demais...')
