import requests
from py_modules import operationsData


def get_url(_type):
    return 'https://pokeapi.co/api/v2/type/' + _type


def get_data_by_type(_type):

    url = get_url(_type)

    pokemon_data = None
    try:
        pokemon_data = requests.get(url)
        pokemon_data.raise_for_status()
    except requests.exceptions.HTTPError as err:
        print(
            f'\nOps! Deu problema aí, ó: {err}\nNão deu pra achar pokemón do tipo {_type}, não (ò,..,ó)')

    return pokemon_data


def get_pokemon_by_type(_type):

    print(f'\nBuscando pokemóns do tipo {_type}...')
    pokemon_list = get_data_by_type(_type)
    found_pokemon = None

    if pokemon_list:
        found_pokemon = pokemon_list.json()['pokemon']
        return [item['pokemon'] for item in found_pokemon]

    return found_pokemon


def find_pokemon_data(_pokemon_list, _pokemon_name):

    print(f'\nBuscando {_pokemon_name}...')
    found_pokemon_info = None
    if operationsData.has_pokemon(_pokemon_list, _pokemon_name):

        found_pokemon = next(
            pokemon for pokemon in _pokemon_list if pokemon['name'] == _pokemon_name)

        pokemon_general_id = found_pokemon['url'].split('/')[-2]
        pokemon_list_id = _pokemon_list.index(found_pokemon)

        found_pokemon_info = {
            'name': found_pokemon['name'],
            'url': found_pokemon['url'],
            'general_id': pokemon_general_id,
            'list_id': pokemon_list_id
        }

    return found_pokemon_info
