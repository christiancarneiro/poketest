def has_pokemon(_pokemon_list, _pokemon_name):
    name_list = [item['name'] for item in _pokemon_list]
    return _pokemon_name in name_list


def add_pokemon(_pokemon_list, _pokemon_object):
    print(f'Adicionando {_pokemon_object["name"]}...')
    if not has_pokemon(_pokemon_list, _pokemon_object['name']):
        print('Sucesso!')
        _pokemon_list.append(_pokemon_object)
    else:
        print("A operação falhou. A clonagem de pokemóns não é permitida pela OMS.")
    return _pokemon_list


def remove_pokemon(_pokemon_list, _pokemon_name):
    print(f'Removendo {_pokemon_name}...')
    if has_pokemon(_pokemon_list, _pokemon_name):
        print('Sucesso!!')
        pokemon_object = next(
            item for item in _pokemon_list if item['name'] == _pokemon_name)
        _pokemon_list.remove(pokemon_object)
    else:
        print("A operação falhou. Não se pode perder o que não se tem.")
    return _pokemon_list


def rename_pokemon(_pokemon_list, _old_pokemon_name, _new_pokemon_name):
    print(f'Rebatizando o pokemón {_old_pokemon_name} => {_new_pokemon_name}')
    if has_pokemon(_pokemon_list, _old_pokemon_name):
        print('Sucesso!')
        index = _pokemon_list.index(
            next(item for item in _pokemon_list if item['name'] == _old_pokemon_name))
        _pokemon_list[index]["name"] = _new_pokemon_name
    else:
        print(
            f'A operação falhou. Não existe um pokemón chamado {_old_pokemon_name} na lista fornecida.')
    return _pokemon_list


def order_pokemon_list(_pokemon_list, _order):
    print(f'\nOrdenando a lista em ordem {_order}')
    if _order == "crescente":
        print("Deu certo!")
        _pokemon_list.sort(key=lambda item: item['name'])

    elif _order == "decrescente":
        print("Deu certo!")
        _pokemon_list.sort(key=lambda item: item['name'], reverse=True)
    else:
        print("O processo falhou. Erro de parâmetro.")

    return _pokemon_list


def intersect_pokemon_lists(_first_pokemon_list, _second_pokemon_list):
    print('\nBuscando convergências...')
    return [item for item in _first_pokemon_list if item in _second_pokemon_list]


def unite_pokemon_lists(_first_pokemon_list, _second_pokemon_list):
    print("Unindo pokemóns...")
    merged_list = _first_pokemon_list + _second_pokemon_list
    return list(map(dict, set(tuple(pokemon_object.items())
                              for pokemon_object in merged_list)))


def multiple_intersect_pokemon_lists(*args):
    intersect_lists = args[0]
    for i in range(1, len(args)):
        intersect_lists = intersect_pokemon_lists(intersect_lists, args[i])

    return intersect_lists
