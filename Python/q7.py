from py_modules import getData, outputData, operationsData


first_type = 'dragon'
second_type = 'flying'


def main(_first_type, _second_type):

    first_pokemon_list = getData.get_pokemon_by_type(_first_type)
    second_pokemon_list = getData.get_pokemon_by_type(_second_type)
    if first_pokemon_list and second_pokemon_list:
        double_type_list = operationsData.intersect_pokemon_lists(
            first_pokemon_list, second_pokemon_list)
        outputData.output_pokemon_list(double_type_list)


main(first_type, second_type)
